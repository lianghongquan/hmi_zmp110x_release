# HMI_ZMP110x_release

#### 介绍
立功科技基于AWTK的智能串口屏方案，拖拽式UI设计，可一键打包资源文件，只需极低代码量即可完成与设备的通信交互。编译后将固件拷贝到U盘或SD卡，即可完成升级和显示，基于RTOS系统，实现秒开机。
![输入图片说明](1.png)
#### 方案特点
1. 专业的UI设计引擎AWTK，UI设计更美观，所见即所得，拖拽式开发；
2. 一键打包UI设计源码，低代码开发；
3. 高性能MPU处理器，采用RTOS系统，秒开机；
4. 原理图、PCB参考设计；
5. Demo板评估；
6. 硬件设计极简，主控板不足名片大小；
7. 外设例程与配套文档；
8. 完善demo演示程序；

#### 安装和使用教程

1.  环境安装及开发流程，请参考《ZMP1106串口屏应用开发手册_V1.00》文档；
2.  提供了完善的视频教程，从搭建开发环境，到开发自己的应用，有详细的教程说明；
3.  AWTK的使用和UI设计，可参考https://awtk.zlg.cn/docs提供的资料；

#### 特别注意

1. 当前demo提供的示例是基于HMI_ZMP106串口屏评估板，默认适配的LCD为RGB565屏，分辨率480*800；
1. 如需使用别的屏，需要修改硬件适配；





#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
